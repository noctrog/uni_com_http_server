extern crate serial;

use std::process;
use std::env;
use std::time::Duration;
use std::io::prelude::*;
use serial::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;

fn main() {
    // Abrir puerto de Arduino si se puede
    let args : Vec<String> = env::args().collect();
    // Si no hay el suficiente número de argumentos, salir
    if args.len() == 1 {
        println!("ERROR: Se debe indicar el puerto al que está conectado el Arduino!");
        process::exit(-1);
    }
    // El primer argumento debe de ser el puerto serie
    let mut serial_port = serial::open(&args[1]).unwrap();
    // Si se ha podido abrir el puerto, lo configura
    serial_port.reconfigure(&|settings| {
        (settings.set_baud_rate(serial::Baud115200))?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }).unwrap(); 

    // Tiempo de espera máximo en la conexión serie
    serial_port.set_timeout(Duration::from_millis(1500)).unwrap();

    // Servidor TCP en el puerto 7878, que acepta todas las conexiones
    let listener = TcpListener::bind("0.0.0.0:7878").unwrap();

    for stream in listener.incoming() {
        let stream = stream.unwrap();

        handle_connection(stream, &mut serial_port);
    }
}

fn handle_connection(mut stream: TcpStream, serial : &mut serial::SystemPort) {
    let mut buffer = [0;4096];

    stream.read(&mut buffer).unwrap();

    // Pasar a cadena
    let buffer_str = std::str::from_utf8(&buffer).unwrap();

    // Extraer comando
    let buffer_words : Vec<&str> = buffer_str.split_whitespace().collect();
    let comando = &buffer_words[1][1..];
    println!("Comando recibido: {}", comando);

    if comando == "estado_puerta" {
        // Se envía el comando al arduino
        serial.write(comando.as_bytes()).unwrap();
        // Se recibe la respuesta
        let mut msg = [0;4096];
        serial.read(&mut msg[..]).unwrap();
        let msg_str = std::str::from_utf8(&msg).unwrap();
        // Se envía la respuesta a la aplicación
        let response = format!("HTTP/1.1 200 OK\r\n\r\n{}", msg_str);
        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    } else if comando == "estado_emergencia" {
        // Se envía el comando al arduino
        serial.write(comando.as_bytes()).unwrap();
        // Se recibe la respuesta
        let mut msg = [0;4096];
        serial.read(&mut msg[..]).unwrap();
        let msg_str = std::str::from_utf8(&msg).unwrap();
        // Se envía la respuesta a la aplicación
        let response = format!("HTTP/1.1 200 OK\r\n\r\n{}", msg_str);
        stream.write(response.as_bytes()).unwrap();
        stream.flush().unwrap();
    } else if comando == "activar_cinta" {
        serial.write(comando.as_bytes()).unwrap();
    } else if comando == "desactivar_cinta" {
        serial.write(comando.as_bytes()).unwrap();
    }

    println!("Request: {}", String::from_utf8_lossy(&buffer[..]));
}
